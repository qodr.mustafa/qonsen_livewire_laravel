<?php

namespace App\Http\Livewire;

use Livewire\Component;

class TambahOngkir extends Component
{
    public function render()
    {
        return view('livewire.tambah-ongkir');
    }
}
